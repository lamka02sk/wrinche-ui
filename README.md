<img src="https://gitlab.com/lamka02sk/wrinche/raw/master/public/img/logo/green.png" alt="wrinche UI logo" title="wrinche UI" width="224"> UI

---

> **Notice:** This library is in a very early phase of development and does not contain almost all of the planned components. On the other hand, the foundation is fairly powerful and simple to use, so you can make your own components on top of it. Library was primarily created to be used in upcoming wrinche CMS and CRM systems.

## Installation

You can either download build artifacts and use them as UMD or Common JS or just
use npm to download the package:

`npm install wrinche-ui --save`
`npm install wrinche-ui --save-dev`