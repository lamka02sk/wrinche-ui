const expect = require('chai').expect

import D from './../../../src/helpers/defaults'
import Generator from './../../../src/super-components/Generator'

describe('Generator', function() {

    beforeEach(() => {
        this.Generator = new Generator();
    });

    it('can change the namespace', () => {

        const defaultNamespace = this.Generator.namespace;
        this.Generator.setNamespace('custom-namespace');

        expect(this.Generator.namespace).not.to.equal(defaultNamespace);
        expect(this.Generator.namespace).to.equal('custom-namespace');

    });

    it('adds built-in components', () => {

    });

});