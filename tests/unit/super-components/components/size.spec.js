const expect = require('chai').expect

import Generator from './../../../../src/super-components/Generator'
import Size from './../../../../src/super-components/components/Size'
import { shallowMount } from "@vue/test-utils"

describe('Size', function() {

    beforeEach(() => {
        this.Parent = new Generator();
        this.Size = new Size(this.Parent);
    });

    it('generates properties', () => {

        expect(this.Size.props).not.to.haveOwnProperty('size');
        this.Size.generate();

        // Has Size property
        expect(this.Size.props).to.haveOwnProperty('size');

    });

    it('can check if size property is enabled', () => {

        // Prepare component for mount
        this.Size.generate();
        this.Size.props.sm = { type: Object, default: () => ({}) };
        this.Size.template = '<div></div>';

        // Mount component
        let mounted = shallowMount(this.Size);
        mounted.setProps({
            size: 'normal',
            sm: {
                size: 'normal'
            }
        });

        // Return false when size is disabled
        expect(mounted.vm.isSizeEnabled()).to.be.false;
        expect(mounted.vm.isSizeEnabled('sm')).to.be.false;

        // Mount component with enabled size property
        mounted.setProps({
            size: 'large',
            sm: {
                size: 'small'
            }
        });

        // Return true when size is enabled
        expect(mounted.vm.isSizeEnabled()).to.be.true;
        expect(mounted.vm.isSizeEnabled('sm')).to.be.true;

    });

    it('returns generated object', () => {

        expect(this.Size.generate()).to.deep.equal(this.Size);

    });

});