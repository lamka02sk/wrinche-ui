const expect = require('chai').expect

import Generator from './../../../../src/super-components/Generator'
import Color from './../../../../src/super-components/components/Color'

describe('Color', function() {

    beforeEach(() => {
        this.Parent = new Generator();
        this.Color = new Color(this.Parent);
    });

    it('generates properties', () => {

        expect(this.Color.props).not.to.haveOwnProperty('color');
        this.Color.generate();

        // Has color property
        expect(this.Color.props).to.haveOwnProperty('color');

    });

    it('returns generated object', () => {

        expect(this.Color.generate()).to.deep.equal(this.Color);

    });

});