const expect = require('chai').expect

import Generator from './../../../../src/super-components/Generator'
import Loading from './../../../../src/super-components/components/Loading'

describe('Loading', function() {

    beforeEach(() => {
        this.Parent = new Generator();
        this.Loading = new Loading(this.Parent);
    });

    it('generates properties', () => {

        expect(this.Loading.props).not.to.haveOwnProperty('loading');
        this.Loading.generate();

        // Has color property
        expect(this.Loading.props).to.haveOwnProperty('loading');

    });

    it('returns generated object', () => {

        expect(this.Loading.generate()).to.deep.equal(this.Loading);

    });

});