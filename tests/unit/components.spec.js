const expect = require('chai').expect

import D from './../../src/helpers/defaults'
import components from './../../src/components'

describe('box', () => {

    it('has working isEnabled function', () => {

        expect(components.isEnabled('box')).to.equal(components._components.box.enabled);

        components._components.box.enabled = !components._components.box.enabled;
        expect(components.isEnabled('box')).to.equal(components._components.box.enabled);

    });

    it('can enable / disable components', () => {

        expect(components.isEnabled('box')).to.equal(components._components.box.enabled);

        // Manually disable
        components._components.box.enabled = false;
        expect(components.isEnabled('box')).to.be.false;

        // Enable
        components.enable('box');
        expect(components.isEnabled('box')).to.be.true;

        // Disable
        components.disable('box');
        expect(components.isEnabled('box')).to.be.false;

    });

    it('generates correct component tag', () => {

        expect(components.getComponentTag('box')).to.equal(components._prefix + '-' + D.box.tag);

    });

    it('has editable prefix', () => {

        // Check default prefix
        expect(components.getPrefix()).to.equal(D.components.prefix);

        // Set custom prefix
        components.setPrefix('test');
        expect(components.getPrefix()).to.equal('test');
        expect(components._prefix).to.equal('test');

        // Check custom prefix usage
        expect(components.getComponentTag('box')).to.match(/^test-/);

    });

    it('generates correct component entry file path', () => {

        // Check default path
        delete components._components.box.entry;
        expect(components.getComponentPath('box')).to.equal('box/Main');

        // Set custom entry path
        components._components.box.entry = 'path/Main';
        expect(components.getComponentPath('box')).to.equal('box/path/Main');

    });

    it('has a components object getter', () => {

        expect(components.getComponents()).to.deep.equal(components._components);

    });

});