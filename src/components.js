import D from './helpers/defaults'

export default {

    isEnabled(component) {
        return !!this._components[component].enabled;
    },

    enable(component) {
        this._components[component].enabled = true;
    },

    disable(component) {
        this._components[component].enabled = false;
    },

    setPrefix(prefix) {
        this._prefix = prefix;
    },

    getPrefix() {
        return this._prefix;
    },

    getComponentTag(component) {
        return this._prefix.toLowerCase() + '-' + this._components[component].tag.toLowerCase();
    },

    getComponentPath(component) {
        return component + '/' + (this._components[component].entry || 'Main');
    },

    getComponents() {
        return this._components;
    },

    forEach(callback, enabledOnly = false) {
        Object.entries(this._components).forEach(([name, settings]) => {
            if(!enabledOnly || settings.enabled) {
                callback(this.getComponentTag(name), this.getComponentPath(name), name, settings);
            }
        });
    },

    _prefix: D.components.prefix,
    _components: {
        box: {
            tag: D.box.tag,
            enabled: true
        }
    }

}