import d from '../../helpers/defaults'
import classes from '../../helpers/classes'

export default class {

    constructor(parent) {

        this._parent = parent;
        this.props = {};
        this.computed = {};
        this.methods = {};

    }

    generate() {

        const _parent = this._parent;

        this.props = {
            sm: {
                type: Object,
                default: d[_parent.namespace].sm || d.root.sm
            },
            md: {
                type: Object,
                default: d[_parent.namespace].md || d.root.md
            },
            lg: {
                type: Object,
                default: d[_parent.namespace].lg || d.root.lg
            },
            xl: {
                type: Object,
                default: d[_parent.namespace].xl || d.root.xl
            }
        };

        this.computed = {
            cRenderResponsive() {
                return [
                    Object.keys(this.sm).length,
                    Object.keys(this.md).length,
                    Object.keys(this.lg).length,
                    Object.keys(this.xl).length
                ].some(Boolean);
            }
        };

        this.methods = {
            wGenerateResponsiveClasses() {

                if(!this.cRenderResponsive) {
                    return [];
                }

                const prototype = classes[_parent.namespace] || {};

                return Object.entries({
                    sm: this.sm,
                    md: this.md,
                    lg: this.lg,
                    xl: this.xl
                }).reduce((acc, [breakpoint, props]) => {
                    return [
                        ...acc,
                        ...Object.entries(props).map(([prop]) => {

                            const propPrototype = { ...(classes[prop] || {}), ...prototype[prop] };

                            if(!propPrototype.responsive) {
                                return false;
                            }

                            if(typeof propPrototype.responsive === 'object' && !(~propPrototype.responsive.indexOf(breakpoint))) {
                                return false;
                            }

                            const classDefinition = Object.entries(this.classDefs).find(([_, data]) => {
                                return typeof data === 'object' ? data.some(d => d.prop === prop) : data.prop === prop;
                            });

                            if(Array.isArray(classDefinition[1])) {
                                classDefinition[1] = classDefinition[1].find(d => d.prop === prop);
                            }

                            if(!classDefinition) {
                                return false;
                            }

                            return this.wGenerateClass(classDefinition[0], classDefinition[1], breakpoint);

                        }).filter(Boolean)
                    ];
                }, []);

            },
            wGenerateClasses() {
                return [ ...this.wGenerateDefaultClasses(), ...this.wGenerateResponsiveClasses() ];
            },
        };

        return this;

    }

}