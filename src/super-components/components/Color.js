import d from '../../helpers/defaults'

export default class {

    constructor(parent) {
        this._parent = parent;
        this.props = {};
    }

    generate() {

        this.props = {
            color: {
                type: String,
                default: d[this._parent.namespace].color || d.root.color
            }
        };

        return this;

    }

}