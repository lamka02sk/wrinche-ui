import d from '../../helpers/defaults'

export default class {

    constructor(parent) {
        this._parent = parent;
        this.props = {};
    }

    generate() {

        this.props = {
            loading: {
                type: Boolean,
                default: d[this._parent.namespace].loading
            }
        };

        return this;

    }

}