import d from '../../helpers/defaults'

export default class {

    constructor(parent) {
        this._parent = parent;
        this.props = {};
        this.methods = {};
    }

    generate() {

        this.props = {
            size: {
                type: String,
                default: d[this._parent.namespace].size || d.root.size
            }
        };

        this.methods = {
            isSizeEnabled(responsive) {
                return (responsive ? this[responsive].size : this.size) !== 'normal';
            }
        };

        return this;

    }

}