import d from '../../helpers/defaults'
import classes from '../../helpers/classes'

export default class {

    constructor(parent) {

        this._parent = parent;
        this.props = {};
        this.dataObject = {};
        this.computed = {};
        this.methods = {};

    }

    generate() {

        const _parent = this._parent;

        this.props = {
            tag: {
                type: String,
                default: d[_parent.namespace].tag || d.root.tag
            },
            classes: {
                type: Array,
                default: d.root.classes
            },
            styles: {
                type: Object,
                default: d.root.styles
            }
        };

        this.dataObject = {
            classDefs: {}
        };

        this.computed = {
            cClasses() {
                return [ ...this.wClasses(), ...this.wGenerateClasses(), ...this.classes ];
            },
            cStyles() {
                return { ...this.wStyles(), ...this.styles };
            },
        };

        this.methods = {
            wFindParent(name) {

                let parent = this.$parent;

                while(parent) {

                    if(parent.$options.name === name) {
                        return parent;
                    }

                    parent = parent.$parent;

                }

                return null;

            },
            wGenerateClass(cl, { prop, group, toggle = null, format = null }, responsive = null) {

                if(toggle !== null) {

                    if(toggle.startsWith('Fn.')) {

                        if(!this[toggle.substr(3)](responsive)) {
                            return false;
                        }

                    } else if(!this.$props[toggle]) {
                        return false;
                    }

                }

                let value = responsive ? this.$props[responsive][prop] : this.$props[prop];

                if(format !== null) {
                    format = this[format];
                    if(typeof format === 'function') {
                        value = (format)(value, responsive);
                    }
                }

                let result = cl.replace('{}', value);

                if(responsive !== null) {
                    result = result.replace(/^(is-)/, `${responsive}-is-`);
                    result = result.replace(/^(w-)/, `w-${responsive}-`);
                }

                return result;

            },
            wGenerateDefaultClasses() {
                return Object.entries(this.classDefs).reduce((acc, [cl, data]) => {
                    Array.isArray(data)
                        ? acc.push(data.map(d => this.wGenerateClass(cl, d)).filter(Boolean))
                        : acc.push(this.wGenerateClass(cl, data));
                    return acc;
                }, []).filter(Boolean);
            },
            wGenerateClasses() {
                return this.wGenerateDefaultClasses();
            },
            wClasses() {
                return d[_parent.namespace].classes || d.root.classes();
            },
            wStyles() {
                return d.root.styles();
            }
        };

        return this;

    }

}