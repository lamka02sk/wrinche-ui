import d from '../helpers/defaults'

import Main from './components/Main'
import Responsive from './components/Responsive'
import Size from './components/Size'
import Color from './components/Color'
import Loading from './components/Loading'

export default class {

    constructor() {

        this.namespace = 'root';
        this.mergeable = ['props', 'dataObject', 'computed', 'methods', 'watch'];

        this.Main = Main;
        this.Responsive = Responsive;
        this.Size = Size;
        this.Color = Color;
        this.Loading = Loading;

        this.components = {};
        return this;

    }

    setNamespace(namespace = 'root') {
        this.namespace = namespace;
        return this;
    }

    addComponent(name) {
        this.components[name] = new this[name](this);
        return this.components[name];
    }

    isResponsive() {
        this.addComponent('Responsive');
        return this;
    }

    isColorful() {
        this.addComponent('Color');
        return this;
    }

    isDeferrable() {
        this.addComponent('Loading');
        return this;
    }

    isResizable() {
        this.addComponent('Size');
        return this;
    }

    supportsMain() {
        this.addComponent('Main');
    }

    mergeComponents(result, prototype) {

        prototype.generate();

        Object.entries(prototype).forEach(([key, data]) => {

            if(~this.mergeable.indexOf(key)) {
                return result[key] = {
                    ...result[key],
                    ...data
                };
            }

            result[key] = data;

        });

        return result;

    }

    generate() {

        this.supportsMain();

        let resultComponent = {};
        resultComponent = this.mergeComponents(resultComponent, this.components['Main']);

        Object.keys(this.components).forEach(key => {

            if(key !== 'Main') {
                resultComponent = this.mergeComponents(resultComponent, this.components[key]);
            }

        });

        resultComponent.data = function() {
            return resultComponent.dataObject;
        };

        return resultComponent;

    }

}