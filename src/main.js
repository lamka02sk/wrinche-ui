require('../sass/main.sass')

import components from './components'

components.forEach((tag, path) => {
    const component = require('./components/' + path).default;
    Vue.component(tag, component);
}, true);