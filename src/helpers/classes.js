export default {

    /*
     * Global properties with values applied to all component supporting them
     */

    size: {
        responsive: true
    },

    /*
     * Namespaced properties for particular components
     */

    'col.content': {
        offset: {
            responsive: true
        },
        span: {
            responsive: true
        }
    }

}