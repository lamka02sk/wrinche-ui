export default {

    components: {
        prefix: 'w'
    },

    root: {
        tag: 'div',
        classes: () => [],
        styles: () => ({}),
        color: 'default',
        loading: false,
        size: 'normal',
        sm: () => ({}),
        md: () => ({}),
        lg: () => ({}),
        xl: () => ({})
    },

    box: {
        tag: 'box',
        classes: ['w-box'],
        center: true,
        fluid: false,
        full: false
    },

    row: {
        spacing: 0,
        justify: 'space-between',
        multiline: false,
        align: 'top',
        direction: 'row'
    },

    col: {
        span: 24,
        offset: 0,
        skipping: 0
    },

    'col.content': {
        spacing: 0
    },

    button: {
        tag: 'button',
        role: 'button',
        type: 'normal',
        icon: '',
        dashed: false,
        bordered: false,
        ghost: false,
        negative: false,
        disabled: false,
        round: false,
        square: false,
        sharp: false,
        toggleable: false,
        toggled: false
    },

    buttons: {
        toolbar: false,
        stack: false,
        toggle: false,
        single: false,
        toggled: () => []
    },

    DEFAULT_JUSTIFY: 'space-between',
    DEFAULT_ALIGN: 'top',
    DEFAULT_DIRECTION: 'row',
    DEFAULT_MULTILINE: false,
    DEFAULT_SPACING: 0,

    DEFAULT_ROW_SPAN: 24,
    DEFAULT_COLUMN_OFFSET: 0,
    DEFAULT_COLUMN_SKIPPING: 0,

    DEFAULT_CLASSES: [],
    DEFAULT_STYLES: {},

    // Button
    BUTTON: {
        TAG: 'button',
        ROLE: 'button',
        TYPE: 'normal',
        SIZE: 'normal',
        ICON: '',
        COLOR: 'default',
        BORDERED: false,
        GHOST: false,
        NEGATIVE: false,
        LOADING: false,
        DISABLED: false,
        ROUNDED: false,
        SQUARE: false,
        SHARP: false
    },

    // Button group
    BUTTON_GROUP: {
        TOOLBAR: false,
        STACK: false
    },

    // Badge
    BADGE: {
        TYPE: 'bubble',
        POSITION: 'top right',
        SIZE: 'normal',
        ROUND: true,
        VALUE: null,
        COLOR: 'default'
    },

    // Icon
    ICON: {
        TAG: 'i',
        TYPE: 'fas',
        NAME: null,
        SIZE: 'normal'
    }

}