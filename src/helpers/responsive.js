export default {

    enabled: true,

    breakpoints: {
        // xs: 0 => default
        sm: 580,
        md: 800,
        lg: 1020,
        xl: 1320
    },

    modules: [
        // Components
        'column', 'button', 'badge', 'icon',
        // Helpers
        'display', 'floating', 'margin', 'padding',
        // Disabled
        /* 'overflow', */ /* 'align', */ /* 'direction', */ /* 'justify', */
    ],

    properties: {
        align: {
            prefix: ['align', 'self']
        },
        badge: {
            prefix: ['badge']
        },
        button: {
            prefix: ['button']
        },
        column: {
            prefix: ['column', 'offset'],
            except: ['wrapper']
        },
        direction: {
            prefix: ['direction']
        },
        display: {
            prefix: null,
            props: [
                // Display property
                'hidden', 'inline', 'iblock', 'block', 'flex', 'iflex', 'grid', 'igrid', 'table', 'itable',
                'tcell', 'trow', 'tcol', 'contents',
                // Visibility property
                'visible', 'invisible'
            ]
        },
        floating: {
            prefix: null,
            props: [
                // Float property
                'sink', 'start', 'end'
            ]
        },
        icon: {
            prefix: ['icon']
        },
        justify: {
            prefix: ['justify']
        },
        margin: {
            prefix: ['mar']
        },
        overflow: {
            prefix: ['truncate', 'overflow']
        },
        padding: {
            prefix: ['pad']
        }
    },

    propertiesIndex: [],
    exceptProperties: [],

    createIndex() {

        this.propertiesIndex = [];

        this.modules.forEach(mod => {

            const property = this.properties[mod];

            if(!property.prefix) {

                property.props.forEach(prop => {

                    this.propertiesIndex.push(`w-${prop}`);

                    if(property.except) {
                        property.except.forEach(except => {
                            this.exceptProperties.push(`w-${prop}-${except}`);
                        });
                    }

                });

                return true;

            }

            property.prefix.forEach(pr => {
                this.propertiesIndex.push(`w-${pr}`);
            });

        });

    }

}